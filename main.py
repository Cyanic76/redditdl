import sys, urllib3
from bs4 import BeautifulSoup
from functions.get_gif import dl_gif
from functions.get_image import dl_img, dl_img_link
from functions.get_video import dl_video

# Do we automatically confirm downloads?
autoconfirm_dl = False
if '-y' in sys.argv:
  print("-y: I will automatically download files.")
  autoconfirm_dl = True

# Ask user to input their URL, else display preferred scheme
link = input("Provide a Reddit Post URL:\n")
valid_scheme = "https://www.reddit.com/r/[subreddit]/comments/[id]/[title]"

# Check if link is correct
if not link.startswith("https://www.reddit.com/"):
  print(f"This is not a valid URL. Links should follow this scheme:\n{valid_scheme}")
  exit(-1)

# This is the main function
def get(link):
  # Declare req a first time and inform user
  print("Getting data...")
  r = "placeholder"

  # Try to get HTML from Reddit
  try:
    req = urllib3.PoolManager()
    r = req.request("GET", link)
  except Exception:
    print(f"The GET request did not work.")
    exit(-1)

  # Parse HTML we get from reddit
  html_content = r.data.decode("utf-8")
  soup = BeautifulSoup(html_content, "html.parser")

  # Get all images
  img_elements = soup.find_all("img")
  figure_elements = soup.find_all("figure")
  video_elements = soup.find_all("shreddit-player")

  # Gallery
  if len(figure_elements) > 0:
    # How many images/videos/GIFs are there?
    print(f"This gallery post has {len(figure_elements)} files.")
    i = 0
    for tag in figure_elements:
      # Increment i, which is the number of the current file.
      i = i+1
      # Here, tag is a bs4.element.Tag that has a faceplate-img within a figure element.
      # We'll only keep the image link which is in the src tag.
      image_url = str(tag).split("src=\"")[1].split("\"")[0]
      # If it's a video or a GIF, we don't support that.
      if image_url.find("mp4") >= 1 or image_url.find("m3u8") >= 1 or image_url.find("gif") >= 1:
        print(f"File {i} is an unsupported GIF or video within a Gallery.")
      else:
        dl_img_link(img_url=image_url)

  # Image
  elif len(img_elements) >= 2 and not img_elements[0]["src"].startswith("https://external-preview.redd.it"):
    dl_img(img_elements)

  # Video or GIF
  elif len(video_elements) > 0:
    # GIF
    if str(video_elements).find("gif") >= 0:
      dl_gif(video_elements, autoconfirm_dl)
    else:
      # Video
      dl_video(video_elements)

  # Other
  # I am currently intentionally breaking support for link posts.
  else:
    print("Unsupported type of post. It may be a link, poll or text post.")

# Call the above (main) function
get(link)