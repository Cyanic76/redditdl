import urllib3

def dl_video(elements):
  # Get the first element
  video = elements[0]
  # Get the URL with 720 quality
  video_url = str(video).split("preview=\"")[1].split("\"")[0].replace("DASH_96", "DASH_720")
  # Start download
  dl_video_link(video_url)

def dl_video_link(video_url):
  # Ask to download first, so the user is aware we're downloading it
  print(f"\nVideo URL: {video_url}.")
  confirm = input("Download it? ").lower()

  # Download the video
  if confirm == 'y' or confirm == 'yes':
    print("\nDownloading...")
    vid_req = urllib3.PoolManager()
    vid_get = vid_req.request('GET', video_url)
    vid_name = video_url.split('/')[-1].split('?', 1)[0]
    # Open file as binary file and write result in it
    with open(f"./{vid_name}", 'wb') as f:
      f.write(vid_get.data)
    # Inform user we've downloaded the file
    print(f"Saved {vid_name}.")
  else:
    print("\nNot downloading video.")
