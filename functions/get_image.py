import urllib3

# dl_img: Download images from the img_elements value
def dl_img(img_elements):
  # It seems we're getting the same link twice. We'll keep the first one.

  # Get the image URL
  img_url = img_elements[0]['src']

  # Call the other function
  dl_img_link(img_url)

# dl_img_link: Download image from its link
def dl_img_link(img_url):
  # Ask to download first, so the user is aware we're downloading it
  print(f"\nImage URL: {img_url}.")
  confirm = input("Download it? ").lower()

  # Download the image
  if confirm == 'y' or confirm == 'yes':
    print("\nDownloading...")
    img_req = urllib3.PoolManager()
    img_get = img_req.request('GET', img_url)
    img_name = img_url.split('/')[-1].split('?', 1)[0]
    # Open file as binary file and write result in it
    with open(f"./{img_name}", 'wb') as f:
      f.write(img_get.data)
    # Inform user we've downloaded the file
    print(f"Saved {img_name}.")
  else:
    print("\nNot downloading image.")
