import urllib3
from io import BytesIO
from PIL import Image

def dl_gif(elements, autoconfirm_dl):
  # Get the first element
  gif = elements[0]
  # Get the GIF
  gif_url = str(gif).split("src=\"")[1].split("\"")[0].replace("amp;", "")
  # Download
  dl_gif_link(gif_url, autoconfirm_dl)

def dl_gif_link(gif_url, autoconfirm_dl):
  # Ask to download first, so the user is aware we're downloading it
  print(f"\nGIF URL: {gif_url}.")
  confirm = 'n' # Temporary variable
  if autoconfirm_dl == False or not autoconfirm_dl:
    confirm = input("Download it? ").lower()

  # Download the video
  if confirm == 'y' or confirm == 'yes' or autoconfirm_dl == True:
    print("\nDownloading...")
    gif_req = urllib3.PoolManager()
    gif_get = gif_req.request('GET', gif_url)
    gif_name = gif_url.split('/')[-1].split('?', 1)[0]
    # We're getting 403s, somehow. Let's tell the end user.
    if str(gif_get.data).find("403 Forbidden") > 0:
      print("403: Can't download GIF. Trace:")
      print(gif_get.data)
      exit(-1)
    # Save the file using Image from PIL
    gif_data = BytesIO(gif_get.data)
    with open(f"{gif_name}", "wb") as f:
      f.write(gif_data.read())
    # Inform user we've downloaded the file
    print(f"Saved {gif_name}.")
  else:
    print("\nNot downloading GIF.")