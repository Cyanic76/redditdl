# redditdl

Download **static** images from Reddit.

## Install

You need to install the `bs4` and `urllib3` packages:

```sh
pip3 install bs4 urllib3
```

Clone this repository or just download the `main.py` file.

That's it!

---

## Use redditdl

Run the script:

```sh
python main.py
```

Provide a valid Reddit Post URL, such as [this one](https://r.cyanic.me/ad?w=https://www.reddit.com/r/meme/comments/14ops6d/the_most_honorable_animal/).

Download the image by typing `y` or `yes` and `ENTER`.

---

## Reporting issues

When opening issues about errors, provide the **full traceback** and your operating system.

I don't provide support to Windows/MacOS users.

### Known issues

- This script doesn't work with posts that are/contain:
  - [Gallery posts](https://r.cyanic.me/ad?w=https://www.reddit.com/r/announcements/comments/hrrh23/now_you_can_make_posts_with_multiple_images/) that includes GIFs and/or videos
  - Link posts (includes Imgur and YouTube links)

### Currently working

- GIF and/or video download
- Post type detection
- Static image download
- Static image download within gallery posts

---
by [Cyanic76](https://cyanic.me) - Released under CC0 1.0 Universal.